import { Moment } from 'moment';

export interface IConference {
  id?: number;
  name?: string;
  participants?: number;
  date?: Moment;
}

export const defaultValue: Readonly<IConference> = {};
