import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './conference.reducer';
import { IConference } from 'app/shared/model/conferences/conference.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IConferenceProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Conference = (props: IConferenceProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { conferenceList, match } = props;
  return (
    <div>
      <h2 id="conference-heading">
        Conferences
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Conference
        </Link>
      </h2>
      <div className="table-responsive">
        {conferenceList && conferenceList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Participants</th>
                <th>Date</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {conferenceList.map((conference, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${conference.id}`} color="link" size="sm">
                      {conference.id}
                    </Button>
                  </td>
                  <td>{conference.name}</td>
                  <td>{conference.participants}</td>
                  <td>
                    <TextFormat type="date" value={conference.date} format={APP_DATE_FORMAT} />
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${conference.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${conference.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${conference.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <div className="alert alert-warning">No Conferences found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ conference }: IRootState) => ({
  conferenceList: conference.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Conference);
